## 0.7.0 (2022-04-30)

### 📦 Dependency updates (1 change)

- [Update Helm release mongodb to v12](dependabot-gitlab/chart@4abbf455578d3d340bce90bb0d7194c9baa88704) by @dependabot-bot. See merge request dependabot-gitlab/chart!78

## 0.6.0 (2022-04-30)

### 🚀 Features (1 change)

- [Configurable ignored sentry error list](dependabot-gitlab/chart@e2d31d8700aacb275ad0fb44ebb29c0ac8f9e988) by @andrcuns. See merge request dependabot-gitlab/chart!75

### 🔬 Improvements (1 change)

- [Add project hook creation option](dependabot-gitlab/chart@9a398e5505cbd95cf5b73e1eca85924ac77cb9c1) by @sebbrandt87. See merge request dependabot-gitlab/chart!77

### 🔧 CI changes (1 change)

- [Update dependency docker to v20.10.14](dependabot-gitlab/chart@3f538f175e1b4601b9e1684a0c959c339a26a2ef) by @dependabot-bot. See merge request dependabot-gitlab/chart!71

### 📦 Dependency updates (3 changes)

- [Update app version to v0.19.0](dependabot-gitlab/chart@afd844ca78be2a02ea506366c3e9964dad60f173) by @dependabot-bot.
- [Update Helm release redis to ~16.8.0](dependabot-gitlab/chart@9e667d1f0cff1a15da57312d1286475f0095410b) by @dependabot-bot. See merge request dependabot-gitlab/chart!74
- [Update Helm release common to ~1.13.0](dependabot-gitlab/chart@2bcfea9a136dae3371f7c938d2e9d41f5025185a) by @dependabot-bot. See merge request dependabot-gitlab/chart!72

## 0.5.10 (2022-04-22)

### 📦 Dependency updates (1 change)

- [Update app version to v0.18.0](dependabot-gitlab/chart@0cfbf724e4853bb01536200460688700ebd6a6eb) by @dependabot-bot.

## 0.5.9 (2022-04-09)

### 📦 Dependency updates (3 changes)

- [Update app version to v0.17.2](dependabot-gitlab/chart@55ac11552bc085f8647161ccfcddde234f2cc9c5) by @dependabot-bot.
- [Update app version to v0.17.1](dependabot-gitlab/chart@ed2a343cfbe856c8f3a1c5585ae1bc8053d577a0) by @dependabot-bot.
- [Update app version to v0.17.0](dependabot-gitlab/chart@9a16b0b041baf7c28b40d3385baa36581c136a07) by @dependabot-bot.

## 0.5.8 (2022-03-18)

### 🛠️ Chore (1 change)

- [Mount registries secrets in to migration job](dependabot-gitlab/chart@32de9f9dae9f500114daeba1620c6efc636ef533) by @andrcuns. See merge request dependabot-gitlab/chart!69

## 0.5.7 (2022-03-16)

### 📦 Dependency updates (1 change)

- [Update app version to v0.16.0](dependabot-gitlab/chart@ff6d65ac1d65a222c40b54f9f11f34a02df23c3c) by @dependabot-bot.

## 0.5.6 (2022-03-13)

### 🚀 Features (1 change)

- [Add colorized logs option](dependabot-gitlab/chart@f442644ac0b3e6173be272963b5fba2c03159553) by @andrcuns. See merge request dependabot-gitlab/chart!68

## 0.5.5 (2022-03-12)

### 📦 Dependency updates (2 changes)

- [Update app version to v0.15.3](dependabot-gitlab/chart@03d47fe56c0c8ee5b80a505f6361ddebdb04708d) by @dependabot-bot.
- [Update app version to v0.15.2](dependabot-gitlab/chart@88f9a1a90f90bc58035a6e3e5bb5585190a1696a) by @dependabot-bot.

## 0.5.4 (2022-02-21)

### 📦 Dependency updates (1 change)

- [Update app version to v0.15.1](dependabot-gitlab/chart@f3b0642b13301876a8dc181db3f8b0bd299cfaf4) by @dependabot-bot.

## 0.5.3 (2022-02-21)

### 📦 Dependency updates (1 change)

- [Update app version to v0.15.0](dependabot-gitlab/chart@31a4efcf981ecbf17ecf7c1cf835c2fff13f53e5) by @dependabot-bot.

## 0.5.2 (2022-02-16)

### 📦 Dependency updates (1 change)

- [Update app version to v0.14.2](dependabot-gitlab/chart@78bac4b10ab7f0da69ce1e56459a27006b8696d0) by @dependabot-bot.

## 0.5.1 (2022-02-11)

### 📦 Dependency updates (1 change)

- [Update app version to v0.14.1](dependabot-gitlab/chart@628982847c509f8197ae12cd2ff256495a5efdda) by @dependabot-bot.

## 0.5.0 (2022-02-10)

### 🐞 Bug Fixes (1 change)

- [[BREAKING] Store secret key base in secrets object](dependabot-gitlab/chart@bdad0dcb04895444c9926fbce374c2967f46f58e) by @andrcuns. See merge request dependabot-gitlab/chart!67

### 📦 Dependency updates (1 change)

- [Update app version to v0.14.0](dependabot-gitlab/chart@a4bc85121e712d49153869ae69110bc54d3224fa) by @dependabot-bot.

## 0.4.1 (2022-02-06)

### 🐞 Bug Fixes (1 change)

- [Add missing registries credentials for project registration job](dependabot-gitlab/chart@105d5bfb66107e5e48196fe6cc2c49fde0b28acb) by @andrcuns. See merge request dependabot-gitlab/chart!66

## 0.4.0 (2022-01-30)

### 🐞 Bug Fixes (1 change)

- [Fix conditions for db and secrets checksums](dependabot-gitlab/chart@24bb9cf2b9e9eb337fb5e3cf2422998791459df8) by @andrcuns. See merge request dependabot-gitlab/chart!61

### 🔧 CI changes (3 changes)

- [Use versioned ci image](dependabot-gitlab/chart@a6c0aa67f861b1d9a81d79bd757af854f43d9242) by @andrcuns. See merge request dependabot-gitlab/chart!65
- [Add ability to override install values for upgrade tests](dependabot-gitlab/chart@c3d990ef583bb54ac526c078bb0efc5174519d19) by @andrcuns. See merge request dependabot-gitlab/chart!64
- [Modify version before upgrade test](dependabot-gitlab/chart@3b9de934e8aabc255503ba0cb3b8ce08fe884bd3) by @andrcuns. See merge request dependabot-gitlab/chart!62

### 📦 Dependency updates (2 changes)

- [Bump mongodb chart version to 11.0.0](dependabot-gitlab/chart@13aaa4acdfad142ef8d48c7145633b648a2c1130) by @andrcuns. See merge request dependabot-gitlab/chart!59
- [Bump redis chart version to 16.2.0](dependabot-gitlab/chart@305860f0b82fd57368d8d91985d290a226519310) by @andrcuns. See merge request dependabot-gitlab/chart!60

## 0.3.3 (2022-01-30)

### 🐞 Bug Fixes (2 changes)

- [Correctly set redis url depending on configuration](dependabot-gitlab/chart@28a31f37fbad8645c1b64736cccdc00130d860ed) by @andrcuns. See merge request dependabot-gitlab/chart!56
- [Use correct attributes for mongodb checksum](dependabot-gitlab/chart@e0778baa6b01789c798c7780bb1931fe58749c74) by @andrcuns. See merge request dependabot-gitlab/chart!54

### 🔧 CI changes (3 changes)

- [Add tests with ingress configuration](dependabot-gitlab/chart@ba65f4a0cc96e4ad1a275d2010293c9c8daa5b1a) by @andrcuns. See merge request dependabot-gitlab/chart!57
- [Use helm lint instead of chart-testing for linting](dependabot-gitlab/chart@68f5cd89b83df6050c2f73924a6483595ac9318d) by @andrcuns. See merge request dependabot-gitlab/chart!55
- [Fetch previous release from git in upgrade test](dependabot-gitlab/chart@4ad960e73439c11b56eb4746f9f4428b652c7cad) by @andrcuns. See merge request dependabot-gitlab/chart!53

## 0.3.2 (2022-01-29)

### 🔬 Improvements (1 change)

- [Do not create empty secrets object when registries credentials are not defined](dependabot-gitlab/chart@828ccf49d72c918b80dc83144f74b2e6602da614) by @andrcuns. See merge request dependabot-gitlab/chart!50

### 🔧 CI changes (2 changes)

- [Add upgrade job](dependabot-gitlab/chart@449b7f39ad0e66e2156a427e31ecf4716e9e562a) by @andrcuns. See merge request dependabot-gitlab/chart!52
- [Add install test with existing secrets config](dependabot-gitlab/chart@4ce79922c8400b74013f84cd2c9a2112d03934f8) by @andrcuns. See merge request dependabot-gitlab/chart!51

## 0.3.1 (2022-01-28)

### 🐞 Bug Fixes (1 change)

- [fix secretKeyRef in web and worker deployment](dependabot-gitlab/chart@b205c229c5d2a24112947dfa8a03b1ab3b3fd948) by @stieglma. See merge request dependabot-gitlab/chart!48

## 0.3.0 (2022-01-26)

### 🚀 Features (2 changes)

- [Add existing secrets option for credentials and registries credentials](dependabot-gitlab/chart@afb30f07e0d1491b3de1d23d8402c73fd4082f16) by @stieglma. See merge request dependabot-gitlab/chart!47
- [Deployment annotations support for web and worker](dependabot-gitlab/chart@d05f1398950cb42396a1faa79e6cfc079b705018) by @radekgrebski. See merge request dependabot-gitlab/chart!46

### 🔬 Improvements (3 changes)

- [[BREAKING] Correctly handle existing secrets for mongodb and redis](dependabot-gitlab/chart@ee990654875cd0337b27bdf70f96da81ae61ea54) by @andrcuns. See merge request dependabot-gitlab/chart!41
- [Add option to provide custom serviceAccount name](dependabot-gitlab/chart@885fc126b0678da255b7cd0cf39da72e39ab294f) by @andrcuns. See merge request dependabot-gitlab/chart!40
- [[BREAKING] Add bitnami common helpers](dependabot-gitlab/chart@c1ddf8f92f71556ad33b46594cc4329c0aad31dc) by @andrcuns. See merge request dependabot-gitlab/chart!39

### 🔧 CI changes (3 changes)

- [Add gitlab mock to test setup](dependabot-gitlab/chart@5feb976c93d875f00ba829ab0f2d213e98ec0a2d) by @andrcuns. See merge request dependabot-gitlab/chart!44
- [Improve kubeval log output](dependabot-gitlab/chart@47ee6ed865b0cf424d9def8c77d3d09aeefac38b) by @andrcuns. See merge request dependabot-gitlab/chart!43
- [Add install test with different values files](dependabot-gitlab/chart@edf2070cd00e3d3a9261065c603ad9b3c5231ada) by @andrcuns. See merge request dependabot-gitlab/chart!42

### 📦 Dependency updates (1 change)

- [Update app version to v0.13.0](dependabot-gitlab/chart@af28f1693da910b6c50b526810bf6c2446f4b236) by @dependabot-bot.

### fix (1 change)

- [Set correct default values for extra volumes and env vars](dependabot-gitlab/chart@e0b1f3ec2e80d16f7cb67399d14fc2b0f93cfbf6) by @andrcuns. See merge request dependabot-gitlab/chart!45

## 0.2.4 (2021-12-21)

### 📦 Dependency updates (1 change)

- [Update app version to v0.12.0](dependabot-gitlab/chart@f3d9525e4870620c846c38476de6f9a3103ef2bc) by @dependabot-bot.

## 0.2.3 (2021-12-18)

### 🐞 Bug Fixes (1 change)

- [Fixes extraEnvVars](dependabot-gitlab/chart@be30d9e0ef95187f7b3beebe7d82e61446bd92e0) by @christophefromparis. See merge request dependabot-gitlab/chart!37

### 🔧 CI changes (1 change)

- [Remove ci image build](dependabot-gitlab/chart@9490b92b6e9c7cb48fd6fd89cd5f7ba6e3e24147) by @andrcuns. See merge request dependabot-gitlab/chart!36

### 📦 Dependency updates (1 change)

- [Update app version to v0.11.0](dependabot-gitlab/chart@0bcf63fa0a5ab3c41455d05a7c43ecb21ba15cfe) by @dependabot-bot.

## 0.2.2 (2021-12-10)

### 🔬 Improvements (1 change)

- [Add configurable secret key base](dependabot-gitlab/chart@b30deb1a1f5f30b7a5688d165ec85dc0a8312387) by @andrcuns. See merge request dependabot-gitlab/chart!35

## 0.2.1 (2021-12-07)

### 🔧 CI changes (3 changes)

- [Update changelog template](dependabot-gitlab/chart@b60beb793d3dc65044fb89c97ac041641b7ed342) by @andrcuns.
- [Fix gitlab release creation](dependabot-gitlab/chart@b6e2853b6ac61b5a39661d511250ffce95f1bded) by @andrcuns.
- [Fix changelog script](dependabot-gitlab/chart@bc531b718488d3f1ab205ae2673971d0b6183dfc) by @andrcuns. See merge request dependabot-gitlab/chart!34

### 📦 Dependency updates (1 change)

- [Update app version to v0.10.11](dependabot-gitlab/chart@8e11a361f54e456a7ee13a60840ac4cd45f8104f) by @dependabot-bot.

## 0.2.0 (2021-11-18)

### 🔧 CI changes (1 change)

- [Remove docker tag from install job](dependabot-gitlab/chart@c09a964c51084e5ceeda03b1e69b67bf54daf777) ([merge request](dependabot-gitlab/chart!28))

### 📦 Dependency updates (2 changes)

- [Bump mongodb chart version](dependabot-gitlab/chart@8c4e504ed226e54c14a5176e5616e18135f92a39) ([merge request](dependabot-gitlab/chart!29))
- [Bump redis chart version](dependabot-gitlab/chart@14963d45e8aaaa08c36cb4bb2d529dbcce46cfef) ([merge request](dependabot-gitlab/chart!27))

## 0.1.3 (2021-11-18)

### 🚀 Features (1 change)

- [Update app version to v0.10.9](dependabot-gitlab/chart@af3942f30ea1c7f04cce38cd4c51e29bca6987b9)

### 🔧 CI changes (1 change)

- [Always build chart](dependabot-gitlab/chart@0099c19fcba4d1d418f2f53e2401c9ae66598b58) ([merge request](dependabot-gitlab/chart!24))

### 🛠️ Chore (1 change)

- [Publish logo in pages](dependabot-gitlab/chart@892da0d186996cb1eb8e56ff9a83d2a2e6044f31) ([merge request](dependabot-gitlab/chart!26))

## 0.1.2 (2021-11-16)

### 🔧 CI changes (2 changes)

- [Package readme in license with chart](dependabot-gitlab/chart@c1d1624556170c40961f40e6a59d26c832fe9ad8) ([merge request](dependabot-gitlab/chart!21))
- [Install grep in CI image](dependabot-gitlab/chart@114ea966b6085729a4e85f2447a0020c52f616a5) ([merge request](dependabot-gitlab/chart!20))

### 🛠️ Chore (2 changes)

- [Fix logo url](dependabot-gitlab/chart@965d68c20bddf959e5c20c22ecc962e6c4537e2b) ([merge request](dependabot-gitlab/chart!23))
- [Add artifacthub repo info](dependabot-gitlab/chart@2565e97eb4ddcdfe86426459099b0549bfdc37b1) ([merge request](dependabot-gitlab/chart!22))

## 0.1.1 (2021-11-16)

### 🐞 Bug Fixes (1 change)

- [Generate tag specific migration job name](dependabot-gitlab/chart@aa94d111306266d4ae0ce7e9e22d5ced5b497d84) ([merge request](dependabot-gitlab/chart!17))

### 🔧 CI changes (2 changes)

- [Generate changelog for releases](dependabot-gitlab/chart@9b391ee0238a029584981cebaf164a454fe36e0e) ([merge request](dependabot-gitlab/chart!15))
- [Do not publish chart if tests failed](dependabot-gitlab/chart@daed1ccbc9ef790375c0647f848f580a9a3a863b) ([merge request](dependabot-gitlab/chart!14))

### 🛠️ Chore (1 change)

- [Shorten job names](dependabot-gitlab/chart@9e4b7de92e12c95cd22dbc1396016fb90c49d453) ([merge request](dependabot-gitlab/chart!19))

### 📄 Documentation updates (1 change)

- [Update value documentation and default values](dependabot-gitlab/chart@4ee7c24998f8435665314a907b9231918f09148d) ([merge request](dependabot-gitlab/chart!16))

## 0.1.0 (2021-11-14)

### 🛠️ Chore (1 change)

- [Update release tag format and fix release script](dependabot-gitlab/chart@d740a28ee1698189511a9c3293820fc53baf08e5) ([merge request](dependabot-gitlab/chart!13))

### 📄 Documentation updates (1 change)

- [Update chart url in documentation](dependabot-gitlab/chart@94478e14d2fc75a2c1bb17b8aee8d8d9b110b1b9) ([merge request](dependabot-gitlab/chart!12))

### CI (1 change)

- [Add chart release jobs and utils](dependabot-gitlab/chart@ea1a45999523d3cd9bedb53ddbf2290e022072bd) ([merge request](dependabot-gitlab/chart!11))
