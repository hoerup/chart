#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

log "Publish helm chart"
gsutil -o "Credentials:gs_service_key_file=${GCLOUD_KEYFILE}" cp dependabot-gitlab-*.tgz gs://dependabot-gitlab
