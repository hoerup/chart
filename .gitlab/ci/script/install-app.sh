#!/bin/bash

set -eo pipefail

source "$(dirname "$0")/utils.sh"

function helm-install() {
  command="${1:-install}"
  dir="${2:-$CHART_DIR}"
  values="${3:-.gitlab/ci/kube/values/${VALUES}.yaml}"
  override_values=".gitlab/ci/kube/override/${VALUES}.yaml"

  if [[ "$command" != "diff upgrade" ]]; then
    extra_args="--timeout 8m --wait --wait-for-jobs"
  else
    extra_args="--allow-unreleased --show-secrets --color --context 5"
  fi

  if [[ "$command" == "install" && "$CI_JOB_NAME" == "upgrade"* ]] && [[ -f "$override_values" ]]; then
    override_arg="--values ${override_values}"
  fi

  helm $command "$RELEASE_NAME" "$dir" \
    --set env.gitlabUrl="http://gitlab-smocker.gitlab.svc.cluster.local:8080" \
    --namespace "$NAMESPACE" \
    --values "$values" \
    $override_arg \
    $extra_args
}

release_version="v$(helm local-chart-version get -c charts/dependabot-gitlab)"
release_version_chart_dir="${release_version}/${CHART_DIR}"
values_yml=".gitlab/ci/kube/values/${VALUES}.yaml"
release_version_values_yml="${release_version}/${values_yml}"

log2 "Add bitnami repo"
helm repo add bitnami https://charts.bitnami.com/bitnami

if [[ "$CI_JOB_NAME" == "upgrade"* ]]; then
  log2 "Run helm upgrade"

  log "Fetch previous release"
  git fetch origin tag "$release_version" --no-tags
  git worktree add "$release_version" "$release_version"

  log "Install current release"
  if [[ ! -f "$release_version_values_yml" ]]; then
    warn "Values file '${values_yml}' doesn't exist in release ${release_version}!"
    warn "Skipping running upgrade tests!"
    touch success
    exit
  fi
  helm dependency build "${release_version_chart_dir}" --skip-refresh
  helm-install "install" "${release_version_chart_dir}" "$release_version_values_yml"

  log "Upgrade dependabot release"
  helm local-chart-version set -c "$CHART_DIR" -p "$CI_COMMIT_SHORT_SHA"
  helm dependency build "$CHART_DIR" --skip-refresh
  helm-install "diff upgrade"
  helm-install upgrade

  touch success
else
  log2 "Run helm install"
  helm dependency build "$CHART_DIR" --skip-refresh
  helm-install "diff upgrade"
  helm-install

  touch success
fi
