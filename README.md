# dependabot-gitlab

![Version: 0.7.0](https://img.shields.io/badge/Version-0.7.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.19.1](https://img.shields.io/badge/AppVersion-0.19.1-informational?style=flat-square)

**Homepage:** <https://gitlab.com/dependabot-gitlab/dependabot>

*dependabot-gitlab* is application providing automated dependency updates for gitlab projects.

## Introduction

This chart bootstraps dependabot-gitlab deployment on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

This documentation is rendered from the `main` branch. To see docs for the latest release version, follow this link:
[0.7.0](https://gitlab.com/dependabot-gitlab/chart/-/tree/v0.7.0/README.md)

## Add Helm repo

```bash
helm repo add dependabot https://dependabot-gitlab.gitlab.io/chart
```

## Installing the Chart

Install this chart using:

```bash
helm install dependabot dependabot/dependabot-gitlab --values values.yaml
```

The command deploys dependabot-gitlab on the Kubernetes cluster in the default configuration. The [values](#values) section lists the parameters that can be configured during installation.

### MongoDb and Redis

By default, both `redis` and `mongodb` charts are deployed.
Installation of databases can be disabled by setting `redis.enabled: false` and `mongodb.enabled: false` in `values.yaml`.

**Both redis and mongodb charts require for password to be set, otherwise subsequent upgrades will fail!**

For more information on `mongodb` and `redis` chart configuration options, consult chart documentation:

- https://github.com/bitnami/charts/tree/master/bitnami/mongodb
- https://github.com/bitnami/charts/tree/master/bitnami/redis

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | common | ~1.13.0 |
| https://charts.bitnami.com/bitnami | mongodb | ~12.0.0 |
| https://charts.bitnami.com/bitnami | redis | ~16.8.0 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| createProjectsJob.activeDeadlineSeconds | int | `240` | Job Active Deadline |
| createProjectsJob.backoffLimit | int | `1` | Job Back off limit |
| createProjectsJob.resources | object | `{}` | Create projects job resource definitions |
| credentials.existingSecret | string | `""` | dependabot chart: set a secret name here if you want to manage secrets on your own required keys: [SETTINGS__GITLAB_ACCESS_TOKEN, SECRET_KEY_BASE], optional: [SETTINGS__GITHUB_ACCESS_TOKEN, SETTINGS__GITLAB_AUTH_TOKEN], if `redis.enabled` and `mongodb.enabled` set to false, REDIS_PASSWORD and MONGODB_PASSWORD or MONGODB_URI must be present |
| credentials.github_access_token | string | `""` | Github access token |
| credentials.gitlab_access_token | string | `"test"` | Gitlab access token, required |
| credentials.gitlab_auth_token | string | `""` | Gitlab auth token for webhook authentication |
| credentials.secretKeyBase | string | `"key"` | app key base used for credentials encryption |
| env.appConfigPath | string | `"kube/config"` | Configuration path |
| env.appRootPath | string | `"/home/dependabot/app"` | App root |
| env.commandsPrefix | string | `""` | Dependabot comment command prefix |
| env.createProjectHook | bool | `true` | Enable/disable project hook creation |
| env.dependabotUrl | string | `""` | Optional app url, used for automated webhook creation |
| env.gitlabUrl | string | `"https://gitlab.com"` | Gitlab instance URL |
| env.http_proxy | string | `""` | Enable upstream http proxy |
| env.https_proxy | string | `""` | Enable upstream https proxy |
| env.logColor | bool | `false` | Enhanced colorized log messages |
| env.logLevel | string | `"info"` | App log level |
| env.mongoDbUri | string | `""` | MongoDB URI |
| env.mongoDbUrl | string | `""` | MongoDB URL |
| env.no_proxy | string | `""` | Set proxy exceptions |
| env.redisUrl | string | `""` | Redis URL |
| env.sentryDsn | string | `""` | Optional sentry dsn for error reporting |
| env.sentryIgnoredErrors | list | `[]` | Sentry ignored error list |
| env.sentryTracesSampleRate | string | `""` | Sentry traces sample rate |
| env.updateRetry | int | `2` | Update job retry count or 'false' to disable |
| fullnameOverride | string | `""` | Override fully qualified app name |
| image.imagePullSecrets | list | `[]` | Image pull secrets specification |
| image.pullPolicy | string | `"IfNotPresent"` | Image pull policy |
| image.repository | string | `"docker.io/andrcuns/dependabot-gitlab"` | Image to use for deploying |
| image.tag | string | `""` | Image tag |
| ingress.annotations | object | `{}` |  |
| ingress.enabled | bool | `false` | Enable ingress |
| ingress.hosts | list | `[]` |  |
| ingress.tls | list | `[]` |  |
| metrics.enabled | bool | `false` | Enable metrics endpoint for prometheus |
| metrics.service.type | string | `"ClusterIP"` | Metrics service type |
| metrics.serviceMonitor.additionalLabels | object | `{}` | Additional labels that can be used so ServiceMonitor resource(s) can be discovered by Prometheus |
| metrics.serviceMonitor.enabled | bool | `false` | Enable serviceMonitor |
| metrics.serviceMonitor.honorLabels | bool | `false` | Specify honorLabels parameter to add the scrape endpoint |
| metrics.serviceMonitor.metricRelabelings | list | `[]` | Metrics RelabelConfigs to apply to samples before ingestion |
| metrics.serviceMonitor.relabellings | list | `[]` | Metrics RelabelConfigs to apply to samples before scraping |
| metrics.serviceMonitor.scrapeInterval | string | `"30s"` | Metrics scrape interval |
| metrics.workerPort | int | `9394` | Worker metrics web server port |
| migrationJob.activeDeadlineSeconds | int | `300` | Job Active Deadline |
| migrationJob.backoffLimit | int | `4` | Job Back off limit |
| migrationJob.resources | object | `{}` | Migration job resource definitions |
| mongodb.auth.databases | list | `["dependabot_gitab"]` | MongoDB custom database |
| mongodb.auth.enabled | bool | `true` | Enable authentication |
| mongodb.auth.existingSecret | string | `""` | MongoDB name of an existing secret to be used (optional, will ignore other password setting when used) |
| mongodb.auth.passwords | list | `[]` | MongoDB custom user passwords |
| mongodb.auth.usernames | list | `["dependabot-gitlab"]` | MongoDB custom user username |
| mongodb.enabled | bool | `true` | Enable mongodb installation |
| mongodb.service.ports.mongodb | int | `27017` | Mongodb service port |
| mongodb.updateStrategy.type | string | `"Recreate"` | Deploy strategy |
| nameOverride | string | `""` | Override chart name |
| podSecurityContext | object | `{"fsGroup":1000,"runAsGroup":1000,"runAsUser":1000}` | Security Context |
| project_registration.cron | string | `""` | Cron expression of project registration cron job |
| project_registration.mode | string | `"manual"` | Project registration mode |
| project_registration.namespace | string | `""` | Allowed namespace expression for projects to register |
| projects | list | `[]` | List of projects to create/update on deployment |
| redis.architecture | string | `"standalone"` | Redis architecture. Allowed values: `standalone` or `replication` |
| redis.auth.enabled | bool | `true` | Enable authentication |
| redis.auth.existingSecret | string | `""` | Redis name of an existing secret to be used (optional, will ignore other password setting when used) |
| redis.auth.existingSecretPasswordKey | string | `""` | Redis name of the key in the existing secret where the password is stored (optional, will only be used when existingSecret is set) |
| redis.auth.password | string | `""` | Redis password |
| redis.enabled | bool | `true` | Enable redis installation |
| registriesCredentials | object | `{"existingSecret":""}` | Credentials for private registries Example: PRIVATE_DOCKERHUB_TOKEN: token |
| registriesCredentials.existingSecret | string | `""` | set a secret name here if you want to manage registries credentials on your own |
| service.annotations | object | `{}` | Service annotations |
| service.port | int | `3000` | Service pot |
| service.type | string | `"ClusterIP"` | Service type |
| serviceAccount.annotations | object | `{}` | Service account annotations |
| serviceAccount.automountServiceAccountToken | bool | `true` | Automount service account token |
| serviceAccount.create | bool | `true` | Create service account |
| serviceAccount.name | string | `""` | Service account name |
| web.affinity | object | `{}` | Affinity |
| web.deploymentAnnotations | object | `{}` | Annotations to add to the Deployment |
| web.extraEnvVars | list | `[]` | Extra environment variables |
| web.extraVolumeMounts | list | `[]` | Extra volumeMounts for the pods |
| web.extraVolumes | list | `[]` | Extra volumes |
| web.livenessProbe.enabled | bool | `true` | Enable liveness probe |
| web.livenessProbe.failureThreshold | int | `5` | Liveness probe failure threshold |
| web.livenessProbe.periodSeconds | int | `10` | Liveness probe period |
| web.livenessProbe.timeoutSeconds | int | `2` | Liveness probe timeout |
| web.nodeSelector | object | `{}` | Node selectors |
| web.podAnnotations | object | `{}` | Pod annotations |
| web.replicaCount | int | `1` | Web container replicas count |
| web.resources | object | `{}` | Web container resource definitions |
| web.startupProbe.enabled | bool | `true` | Enable startup probe |
| web.startupProbe.failureThreshold | int | `12` | Startup probe failure threshold |
| web.startupProbe.initialDelaySeconds | int | `10` | Startup probe initial delay |
| web.startupProbe.periodSeconds | int | `10` | Startup probe period |
| web.startupProbe.timeoutSeconds | int | `3` | Startup probe timeout |
| web.tolerations | list | `[]` | Tolerations |
| web.updateStrategy | object | `{"type":"RollingUpdate"}` | Set up strategy for web installation |
| worker.affinity | object | `{}` | Affinity |
| worker.deploymentAnnotations | object | `{}` | Annotations to add to the Deployment |
| worker.extraEnvVars | list | `[]` | Extra environment variables |
| worker.extraVolumeMounts | list | `[]` | Extra volumeMounts for the pods. This can be used for a netrc by mounting a secret at `/home/dependabot/.netrc`. This can be used for various package managers such as gomod |
| worker.extraVolumes | list | `[]` | Extra volumes |
| worker.livenessProbe.enabled | bool | `true` | Enable liveness probe |
| worker.livenessProbe.failureThreshold | int | `2` | Liveness probe failure threshold |
| worker.livenessProbe.periodSeconds | int | `120` | Liveness probe period |
| worker.livenessProbe.timeoutSeconds | int | `3` | Liveness probe timeout |
| worker.nodeSelector | object | `{}` | Node selectors |
| worker.podAnnotations | object | `{}` | Pod annotations |
| worker.probePort | int | `7433` | Health check probe port |
| worker.replicaCount | int | `1` | Worker container replicas count |
| worker.resources | object | `{}` | Worker container resource definitions |
| worker.startupProbe.enabled | bool | `true` | Enable startup probe |
| worker.startupProbe.failureThreshold | int | `12` | Startup probe failure threshold |
| worker.startupProbe.initialDelaySeconds | int | `10` | Startup probe initial delay |
| worker.startupProbe.periodSeconds | int | `5` | Startup probe period |
| worker.startupProbe.timeoutSeconds | int | `3` | Startup probe timeout |
| worker.tolerations | list | `[]` | Tolerations |
| worker.updateStrategy | object | `{"type":"RollingUpdate"}` | Set up strategy for worker installation |
