image:
  # -- Image to use for deploying
  repository: docker.io/andrcuns/dependabot-gitlab
  # -- Image pull policy
  pullPolicy: IfNotPresent
  # -- Image tag
  tag: ""
  # -- Image pull secrets specification
  imagePullSecrets: []

# -- Override chart name
nameOverride: ""
# -- Override fully qualified app name
fullnameOverride: ""

serviceAccount:
  # -- Create service account
  create: true
  # -- Service account name
  name: ""
  # -- Automount service account token
  automountServiceAccountToken: true
  # -- Service account annotations
  annotations: {}

service:
  # -- Service type
  type: ClusterIP
  # -- Service pot
  port: 3000
  # -- Service annotations
  annotations: {}

ingress:
  # -- Enable ingress
  enabled: false
  # Additional ingress annotations
  annotations: {}
  # Ingress hosts definition
  hosts: []
  # Ingress tls
  tls: []

# -- Security Context
podSecurityContext:
  runAsUser: 1000
  runAsGroup: 1000
  fsGroup: 1000

metrics:
  # -- Enable metrics endpoint for prometheus
  enabled: false
  # -- Worker metrics web server port
  workerPort: 9394
  service:
    # -- Metrics service type
    type: ClusterIP
  serviceMonitor:
    # -- Enable serviceMonitor
    enabled: false
    # -- Additional labels that can be used so ServiceMonitor resource(s) can be discovered by Prometheus
    additionalLabels: {}
    # -- Metrics scrape interval
    scrapeInterval: 30s
    # -- Metrics RelabelConfigs to apply to samples before scraping
    relabellings: []
    # -- Metrics RelabelConfigs to apply to samples before ingestion
    metricRelabelings: []
    # -- Specify honorLabels parameter to add the scrape endpoint
    honorLabels: false

web:
  # -- Pod annotations
  podAnnotations: {}
  # -- Annotations to add to the Deployment
  deploymentAnnotations: {}
  # -- Node selectors
  nodeSelector: {}
  # -- Tolerations
  tolerations: []
  # -- Affinity
  affinity: {}
  # -- Web container replicas count
  replicaCount: 1
  # -- Web container resource definitions
  resources: {}
  # -- Set up strategy for web installation
  updateStrategy:
    type: RollingUpdate
  livenessProbe:
    # -- Enable liveness probe
    enabled: true
    # -- Liveness probe failure threshold
    failureThreshold: 5
    # -- Liveness probe period
    periodSeconds: 10
    # -- Liveness probe timeout
    timeoutSeconds: 2
  startupProbe:
    # -- Enable startup probe
    enabled: true
    # -- Startup probe initial delay
    initialDelaySeconds: 10
    # -- Startup probe failure threshold
    failureThreshold: 12
    # -- Startup probe period
    periodSeconds: 10
    # -- Startup probe timeout
    timeoutSeconds: 3
  # -- Extra volumeMounts for the pods
  extraVolumeMounts: []
  # -- Extra volumes
  extraVolumes: []
  # -- Extra environment variables
  extraEnvVars: []

worker:
  # -- Pod annotations
  podAnnotations: {}
  # -- Annotations to add to the Deployment
  deploymentAnnotations: {}
  # -- Node selectors
  nodeSelector: {}
  # -- Tolerations
  tolerations: []
  # -- Affinity
  affinity: {}
  # -- Worker container replicas count
  replicaCount: 1
  # -- Worker container resource definitions
  resources: {}
  # -- Health check probe port
  probePort: 7433
  # -- Set up strategy for worker installation
  updateStrategy:
    type: RollingUpdate
  livenessProbe:
    # -- Enable liveness probe
    enabled: true
    # -- Liveness probe failure threshold
    failureThreshold: 2
    # -- Liveness probe period
    periodSeconds: 120
    # -- Liveness probe timeout
    timeoutSeconds: 3
  startupProbe:
    # -- Enable startup probe
    enabled: true
    # -- Startup probe initial delay
    initialDelaySeconds: 10
    # -- Startup probe failure threshold
    failureThreshold: 12
    # -- Startup probe period
    periodSeconds: 5
    # -- Startup probe timeout
    timeoutSeconds: 3
  # -- Extra volumeMounts for the pods.
  # This can be used for a netrc by mounting a secret at `/home/dependabot/.netrc`.
  # This can be used for various package managers such as gomod
  extraVolumeMounts: []
  # -- Extra volumes
  extraVolumes: []
  # -- Extra environment variables
  extraEnvVars: []

createProjectsJob:
  # -- Job Back off limit
  backoffLimit: 1
  # -- Job Active Deadline
  activeDeadlineSeconds: 240
  # -- Create projects job resource definitions
  resources: {}

migrationJob:
  # -- Job Back off limit
  backoffLimit: 4
  # -- Job Active Deadline
  activeDeadlineSeconds: 300
  # -- Migration job resource definitions
  resources: {}

env:
  # -- Redis URL
  redisUrl: ""
  # -- MongoDB URL
  mongoDbUrl: ""
  # -- MongoDB URI
  mongoDbUri: ""
  # -- Gitlab instance URL
  gitlabUrl: https://gitlab.com
  # -- Optional sentry dsn for error reporting
  sentryDsn: ""
  # -- Sentry traces sample rate
  sentryTracesSampleRate: ""
  # -- Sentry ignored error list
  sentryIgnoredErrors: []
  # -- Optional app url, used for automated webhook creation
  dependabotUrl: ""
  # -- Enable/disable project hook creation
  createProjectHook: true
  # -- Configuration path
  appConfigPath: kube/config
  # -- App root
  appRootPath: /home/dependabot/app
  # -- Dependabot comment command prefix
  commandsPrefix: ""
  # -- Update job retry count or 'false' to disable
  updateRetry: 2
  # -- App log level
  logLevel: "info"
  # -- Enhanced colorized log messages
  logColor: false
  # -- Enable upstream https proxy
  https_proxy: ""
  # -- Enable upstream http proxy
  http_proxy: ""
  # -- Set proxy exceptions
  no_proxy: ""

credentials:
  # -- Gitlab access token, required
  gitlab_access_token: "test"
  # -- Github access token
  github_access_token: ""
  # -- Gitlab auth token for webhook authentication
  gitlab_auth_token: ""
  # -- app key base used for credentials encryption
  secretKeyBase: key
  # -- dependabot chart: set a secret name here if you want to manage secrets on your own
  # required keys: [SETTINGS__GITLAB_ACCESS_TOKEN, SECRET_KEY_BASE], optional: [SETTINGS__GITHUB_ACCESS_TOKEN, SETTINGS__GITLAB_AUTH_TOKEN],
  # if `redis.enabled` and `mongodb.enabled` set to false, REDIS_PASSWORD and MONGODB_PASSWORD or MONGODB_URI must be present
  existingSecret: ""

# -- Credentials for private registries
# Example: PRIVATE_DOCKERHUB_TOKEN: token
registriesCredentials:
  # -- set a secret name here if you want to manage registries credentials on your own
  existingSecret: ""

project_registration:
  # -- Project registration mode
  mode: "manual"
  # -- Cron expression of project registration cron job
  cron: ""
  # -- Allowed namespace expression for projects to register
  namespace: ""

# -- List of projects to create/update on deployment
projects: []

# ref: https://github.com/bitnami/charts/tree/master/bitnami/redis
redis:
  # -- Enable redis installation
  enabled: true
  # -- Redis architecture. Allowed values: `standalone` or `replication`
  architecture: standalone
  auth:
    # -- Enable authentication
    enabled: true
    # -- Redis password
    password: ""
    # -- Redis name of an existing secret to be used (optional, will ignore other password setting when used)
    existingSecret: ""
    # -- Redis name of the key in the existing secret where the password is stored (optional, will only be used when existingSecret is set)
    existingSecretPasswordKey: ""

# ref: https://github.com/bitnami/charts/tree/master/bitnami/mongodb
mongodb:
  # -- Enable mongodb installation
  enabled: true
  auth:
    # -- Enable authentication
    enabled: true
    # -- MongoDB custom database
    databases: [dependabot_gitab]
    # -- MongoDB custom user username
    usernames: ["dependabot-gitlab"]
    # -- MongoDB custom user passwords
    passwords: []
    # -- MongoDB name of an existing secret to be used (optional, will ignore other password setting when used)
    existingSecret: ""
  updateStrategy:
    # -- Deploy strategy
    type: Recreate
  service:
    ports:
      # -- Mongodb service port
      mongodb: 27017
